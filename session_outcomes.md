Your code is political
======================

    -  MozFest2019 session on community-based rather than enterprise-based software development methodologies.
    - Proposed to pursue healthier web-development projects through code and web literacy in opposition to expert-based full-stack maintenance.

Introductions: throw your paper
-------------------------------

This session's scope and goal (7 min)

Attendees were asked to introduce themselves answering the questions:

    - Why did we show up here?

    - What do we have in mind?

Each should throw a paper aiming a cookie box, without standing-up. Some did have that box quite near. That was proposed as a way of situating ourselves: even without knowing it, we were randomly situated in a certain kind of privilege EVERY time.

(NOTE: I probably needed to emphasise much more on that explanation; due to location problems not everyone got in time to the session. Though that broke the ice.)

My thoughts
: I was really thrilled by such a diverse group, but, at the same time, a very strong commitment and ideas related to my proposal. I was really happy to know myh idea was definetely not mine but a community-shared concern. This was solely the set of introductions and we were already on-topic. That was really exciting.


Activity: Brainstorm alert! What is political? (3 min)
------------------------------------------------------

  - Motivation quote shown:
    
    > La política está fundada en el modo de querer-vivir-propio de los seres humanos, que representa finalmente una voluntad-de-vida.
    > -- Enrique Dussel  
    > -- _20 tesis de política_ (Argentina 2012, 23-25)

    > Politics are founded on that way humans want-to-live, that finally represents a will-of-life.
    > -- Enrique Dussel,   
    > -- _20 tesis de política_ (Argentina, 2012, p.23-25)

  - Attendees came up with the following words (some of them):

    - community, rights, decision-making, ideology, consciousness, awareness, change, policy-making

My thoughts
: We all built up a very concise approach to begin with. We were all pretty faimiliar with politically-aware tech. So we wrote those brainstromed words at our paper board haveing them handy for the whole session.  Rather than a warm-up, this worked as a framework. I was feeling pretty comfortable as a facilitator.


Discussion: Group split, frustrating webdev (5 min)
---------------------------------------------------

(5 minutes)

    - Proposed trigger: Last or most memorable frustrating web project development or deployment experience. Could be yours as developer, yours as the one needing a website, or someone elses.

    - We wrote short ideas around our experiences in cards.

    - Then we clustered those cards detecting relations. We came up with a lot of community / social related problems and very few solely technical related problems. Even tech related problems as absurd deadlines that end-up in tons of unmantained code, was all about 

My thoughts
: this took around 10 minutes. But it was really fun. Participants came-up with a huge set of ideas. What was really useful was that concepts I was expecting to appear like: "we did have no money", "we did have no time", were kind of out of the discussion. Basically because they are so evident and constant, that even those received a different approach from us; we were all talking about people. Problems were about people not-being-considered being developers or part of the beneficted communities. Even everytime we mentioned "methodologies", the basic aim of this session, thoughts around them were trivialised in favor of conflict-management, lack of developer and community contact, power abuse from project managers etc. Pretty therapeutic.


Explanation: Where _Your code is political_ came from
-----------------------------------------------------

This session derives from the in-class hard work done by feminist and technologist Irene Soria, who has been done thorough work within academic and activist circles in Mexico, regarding tech, privacy, safety and critical-tech awareness and literacy (i.e. workshops, classes etc.)

We have got beautiful feedback from amazing participants and students through the years. I have worked with Irene in designing courseware and class-plans and so we wrote an essay for guiding MozFest session aims. I grounded and tailored these ideas to build this probably extremely concise session, for gathering thoughts and participation from _mozillians_ around one of _Your code is political_ first possible technical campaign: campaigning for politically-aware developers and strong community-based methodologies for Web projects.

After all those session-coaching sessions, including a movement organization talk, I decided to create an idea and community-thermometer session where we are could share and celebrate our socially-aware tech approach and identity, aiming to build a movement for socially aware webdev methodologies through web-literacy: each project should be a Web masterclass for both communities and developers.


Discussion: design an imaginary project (20 minutes)
----------------------------------------------------

    - Participants were asked to develop an imaginary web development project in pairs considering it to be:

      - Benefecting a non-profit or...
      - A community empowering proposal, or...
      - A project for culture & arts, or...
      - What else?

    - We explained our projects to the group, barely but necessarily speaking about a method to develop it.

    - After they designed their project, they were asked to press ENTER at my laptop, who was running a simple BASH script picking a random country. So, that previously developed project had to be designed at an specific country.

    - Discussion was built around the specifics of developing in that country and what kind of non-colonial, intersectional approach should we have to build that community-based and not developer / paternalistic based.

My thoughs
: it is amazing how many "global south" countries were at the list. We were 8 at the room and only two projects developed in countries from Mercosur, or European Union or any huge economic adscription. Therefore, this let us get through the Deepening face seemlessly.


Deepening: Current methodologies
--------------------------------

    - We really covered this point at the previous discussion. The outcome was: we don't really have to talk about previous methodologies; being agile or whatever was all about understanding an building the project.

    - I only brought the concepts of:

      - Current webdev methodologies: based on models of transformation
      - Most probably in 3-stages:
        1. GET client's needs or whatevers
        2. DEV client's needs of whatevers
        3. GET client's feedback
        4. GOTO 1 (Remember to KILLSIGN / QUIT everything when you feel we can't go on)

Activity: The world upside down
-------------------------------

    - Providing we did have to begin late (11am session was hard), we did a wider talk rather than an specific activity on this:

      - If common 3-staged methodology was not efficient, what would a community based methodology?
      - I proposed the world-upside-down strategy: let us put that 1, 2, 3 in reverse order.
      - Probably renaming it:
        1. IMMERSE into the community to DEVELOP the concept
        2. GET opinions and suggestions as feedback 
        3. CREATE code
        4. GOTO 3, community is developing, no further feedback needed. (Still KILLSIGN / QUIT until resources last, or when goal is achieved. Bugtracking was done during the process.)
      - Use _Participative observation_ anthropological techniques rather than client's needs.
      - Do accompainment not mantainace.

Conclusion: ending quote and comments
-------------------------------------

    > "To build community requires vigilant awareness of the work we must continually do to undermine all the socialization that leads us to behave in ways that perpetuate domination."
    > -- Bell Hooks 
    > -- _A pedagogy of hope_ (USA, 2013)

Building a movement towards better community-based webdev
---------------------------------------------------------

TODO
