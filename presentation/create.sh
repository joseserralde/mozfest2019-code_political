#!/bin/sh

REVEALJS_DIR="http://189.240.62.236:6090/ofcm-show/reveal.js"
#REVEALJS_DIR="../reveal.js"

pandoc -s -t revealjs -V revealjs-url=$REVEALJS_DIR \
  code_political-presentation.md \
  -o index.html 
