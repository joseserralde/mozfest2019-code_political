---
title: Your code is political
subtitle: Intersecctional approaches to Web development and code literacy<br/><span style="font-size:.4em"><a rel="license" href="http://creativecommons.org/licenses/by/4.0/">(CC) Atribución 4.0</a>. José María Serralde, facilitator / Martha Irene Soria<br/>Creative Commons Mexico</span>
autoPlayMedia: true
theme: serif
---

# {data-background-image="img/code_political-banner.png"}

# Welcome!

## Introductions

> - Throw the paper and situate yourself

> - Why are we here?

> - What is our background?

## Brainstorm alert!

> - What is political?

---

<blockquote>
La política está fundada en el modo de querer-vivir-propio de los seres humanos, que representa finalmente una voluntad-de-vida.<br/>
<small>-- Enrique Dussel<br/>
-- _20 tesis de política_ (Argentina 2012, 23-25)</small>
</blockquote>

---

![](img/dussel.jpg)

---

<blockquote>
Politics are founded on that way humans want-to-live, that finally represents a will-of-life.<br/>
<small>-- Enrique Dussel<br/>
-- _20 tesis de política_ (Argentina 2012, p.23-25)</small>
</blockquote>

# Tales from the crypt-ic Web

## Group split alert

> - Last or most memorable frustrating experience for Web dev.
> - Could be yours as developer
> - Yours as the one needing a website
> - Or someone elses.

## Where _Your code is political_ came from.

> - Irene Soria
> - Workshops, classes...
> - Wonderful feedback, wonderful students

## Design an imaginary project

> - Non-profit or
> - Community empowering proposal
> - Culture & arts
> - What else?
> - ...oh, now add a random country where it is based

##  Current Web dev methodologies

> - Based on models of transformation
> - Most probably 3-staged:
> - GET clients X | DEV clients X | GET feedback

##  The world upside-down

> - A community based methodology?
> - Immersion | Feedback | Creation
> - Participative observation

---

![](img/bell_hooks.jpg)

---

<blockquote>
<small> "To build community requires vigilant awareness of the work we must continually do to undermine all the socialization that leads us to behave in ways that perpetuate domination."<br/>
-- Bell Hooks<br/>
-- _A pedagogy of hope_ (USA, 2013)
</blockquote>

## The world upside down

## The end

José María Serralde Ruiz  
contacto\@joseserralde.org  
\@joseserralde  
