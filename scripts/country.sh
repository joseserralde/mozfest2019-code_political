#!/bin/bash

function po() {
  STR1="Hit ENTER to print a random country."
}

function vars() {
  CNTRFILE="countries.txt"
  LINENUM=($(wc -l $CNTRFILE))
}

function loop() {
  while [ INPUT != "q" ]
  do
    read -p "$STR1" INPUT
    clear
    LINESEL=$(( ( RANDOM % $LINENUM )  + 1 ))
    echo
    sed "${LINESEL}q;d" $CNTRFILE
    echo
  done
}

clear
po
vars
loop
