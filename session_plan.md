Your code is political
======================

\#MozFest2019 session on community-based rather than enterprise-based software development methodologies, 

1. Welcome.

This session's scope and goal (3 min)

2. Throw the paper: introductions (5 minutes)

  - Why did we show up here?

  - What do we have in mind?

3. Brainstorm alert! What is political? (3 min) (Activity)

    > La política está fundada en el modo de querer-vivir-propio de los seres humanos, que representa finalmente una voluntad-de-vida.
    > -- Enrique Dussel  
    > -- _20 tesis de política_ (Argentina 2012, 23-25)

    > Politics are founded on that way humans want-to-live, that finally represents a will-of-life.
    > -- Enrique Dussel,   
    > -- _20 tesis de política_ (Argentina, 2012, p.23-25)

4. Group split. 

   - Last or most memorable frustrating experience (5 min) (Discussion)

    - Could be yours as developer, yours as the one needing a website, or someone elses.


5. Where _Your code is political_ came from.

6. Design an imaginary project (20 minutes) (Discussion)

  - Non-profit or
  - Community empowering proposal
  - Culture & arts
  - What else?

4. About _Your code is political_ (Input)

5. Current methodologies (Deepening)

6. The world upside down (Activity)

    - Post - Post its in three stages non-traditional

7. Discussion

    - What will this upsidedown-world would mean?
    - Better things coming.


    > "To build community requires vigilant awareness of the work we must continually do to undermine all the socialization that leads us to behave in ways that perpetuate domination."
    > -- Bell Hooks 
    > -- _A pedagogy of hope_ (USA, 2013)
